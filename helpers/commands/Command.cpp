//
// Created by David on 2/11/2020.
//

#include "Command.h"
#include <iostream>

string Command::getResult() {
    return result;
}

Command::Command(map<string, string> parameters) {
    params = parameters;
}

void Command::printParams() {
    for (auto it = params.begin(); it != params.end(); ++it) {
        std::cout << it->first << " " << it->second << "\n";
    }
}

void Command::setParameter(string key, string *parameter, bool required) {
    if (params.count(key) > 0) {
        *parameter = params.find(key)->second;
    } else if (required) {
        error = true;
        errorMessage = key + " field is required";
    }
}

bool Command::hasError() {
    return error;
}

string Command::getError() {
    return errorMessage;
}

void Command::setSession(Session *pSession) {
    session = pSession;
}