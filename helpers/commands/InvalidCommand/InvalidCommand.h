#ifndef MESSENGER_INVALIDCOMMAND_H
#define MESSENGER_INVALIDCOMMAND_H

using namespace std;

#include "../Command.h"
#include <string>
#include <map>

class InvalidCommand : public Command {
private:

public:
    InvalidCommand(map<string, string> parameters);

    virtual void execute() noexcept override;
};


#endif //MESSENGER_INVALIDCOMMAND_H
