#ifndef MESSENGER_REGISTERCOMMAND_H
#define MESSENGER_REGISTERCOMMAND_H

#include "../Command.h"
#include <string>
#include <map>

class RegisterCommand : public Command {
private:
    string name;
    string email;
    string password;

    bool validateEmail();
    int insertUser();
public:
    RegisterCommand(map<string, string> parameters);

    virtual void execute() noexcept override;
};


#endif //MESSENGER_REGISTERCOMMAND_H
