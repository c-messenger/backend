//
// Created by David on 2/15/2020.
//

#include "ShowOnlineUsersCommand.h"

using namespace std;

ShowOnlineUsersCommand::ShowOnlineUsersCommand(map<string, string> parameters) : Command(parameters) {
}

void ShowOnlineUsersCommand::execute() noexcept {
    for (auto it = session->pOnlineUsers->begin(); it != session->pOnlineUsers->end(); ++it) {
        if(session->userId != it->first)
        result += to_string(it->first) + " => " + it->second.first + '\n';
    }
}
