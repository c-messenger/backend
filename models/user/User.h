//
// Created by David on 2/14/2020.
//

#ifndef MESSENGER_USER_H
#define MESSENGER_USER_H

#include "../Model.h"

class User: public Model {
private:
    string table = "users";

public:
    int create(const map<string, boost::variant<string, int, float>>& data);
    bool update(int id, const map<string, boost::variant<string, int, float>>& data);
    sql::ResultSet *where(const map<string, boost::variant<string, int, float >>& = {});
private:
    unsigned int id;


};


#endif //MESSENGER_USER_H
