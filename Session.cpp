//
// Created by David on 2/14/2020.
//

#include "Session.h"
#include "helpers/CommandResolver.h"
#include <iostream>
#include <boost/bind.hpp>

using namespace std;

Session::Session(boost::asio::io_service &io_service) : socket_(io_service) {

}

boost::asio::ip::tcp::socket &Session::socket() {
    return socket_;
}

void Session::start() {
    readMessage();
    cout << "session started: " << &socket_ << endl << flush;
}

void Session::readMessage() {
    removeData();
    socket_.async_read_some(boost::asio::buffer(data_, max_length),
                            boost::bind(&Session::readCallback, this,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
}

void Session::readCallback(const boost::system::error_code &error, size_t bytes_transferred) {
    if (!error) {
        CommandResolver resolver;
        resolver.resolve(data_);

        Command *command = resolver.getCommand();

        command->setSession(this);

        if (command->hasError()) {
            writeMessage("e: " + command->getError());
        } else {
            command->execute();

            if (command->hasError()) {
                cout << command->getError();
                writeMessage("e: " + command->getError());
            } else {
                writeMessage("s: " + command->getResult());
            }
        }

    } else {
        pOnlineUsers->erase(userId);
        delete this;
    }
}

void Session::sendMessage(const string &messageText) {
    boost::asio::write(socket_, boost::asio::buffer(messageText + '\n', messageText.length() + 1));
}

void Session::writeMessage(const string &messageText) {
    boost::asio::async_write(socket_,
                             boost::asio::buffer(messageText + '\n', messageText.length() + 1),
                             boost::bind(&Session::writeCallback, this,
                                         boost::asio::placeholders::error));
}

void Session::writeCallback(const boost::system::error_code &error) {
    if (!error) {
        readMessage();
    } else {
        delete this;
    }
}

void Session::setOnlineUsers(map<int, pair<string, Session*>> *pOnUsers) {
    pOnlineUsers = pOnUsers;
}

void Session::removeData()
{
    for (int i = 0; i < max_length - 1; ++i)
        data_[i] = '\0';
}

