#include "InvalidCommand.h"

using namespace std;

InvalidCommand::InvalidCommand(map<string, string> parameters) : Command(parameters) {
}

void InvalidCommand::execute() noexcept {
    result = "Invalid command";
}

