#include "LoginCommand.h"
#include "mysql_connection.h"
#include "../../../models/user/User.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cstring>


using namespace std;

LoginCommand::LoginCommand(map<string, string> parameters) : Command(parameters) {
    setParameter("email", &email, true);
    setParameter("password", &password, true);
}

void LoginCommand::execute() noexcept {
    User user;

    sql::ResultSet *res = user.where({
                                             {"email",    email},
                                             {"password", password}
                                     });

    if (res->next()) {
        session->pOnlineUsers->insert({
                                              stoi(res->getString("id")),
                                              {res->getString("name"), session}
                                      });
        session->userId = stoi(res->getString("id"));
        session->userName = res->getString("name");

        // send online users to all users
        for (auto it = session->pOnlineUsers->begin(); it != session->pOnlineUsers->end(); ++it) {
            if (it->first != stoi(res->getString("id"))) {

                string onlineUsers;
                for (auto it2 = session->pOnlineUsers->begin(); it2 != session->pOnlineUsers->end(); ++it2) {
                    if(it->first != it2->first)
                        onlineUsers += to_string(it2->first) + " => " + it2->second.first + '\n';
                }

                it->second.second->sendMessage("o: " + onlineUsers);
            }
        }
        result = "You are successfully logged in";
    } else {
        error = true;
        errorMessage = "Invalid Email or Password";
    }
    delete res;
}

