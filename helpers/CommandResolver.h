//
// Created by David on 2/11/2020.
//

#ifndef MESSENGER_COMMANDRESOLVER_H
#define MESSENGER_COMMANDRESOLVER_H

#include <string>
#include <map>
#include "commands/Command.h"

using namespace std;

class CommandResolver {
private:
    string command;
    map<string, string> params;
public:

    /*
     * Ex. "login -email test@test.test -password testtest"
     * will set command to "login" and map parameters to params
     */
    void resolve(string command);

    Command* getCommand();
};


#endif //MESSENGER_COMMANDRESOLVER_H
