#ifndef MESSENGER_LOGINCOMMAND_H
#define MESSENGER_LOGINCOMMAND_H

using namespace std;

#include "../Command.h"
#include <string>
#include <map>

class LoginCommand : public Command {
private:
    string email;
    string password;
public:
    LoginCommand(map<string, string> parameters);

    virtual void execute() noexcept override;
};


#endif //MESSENGER_LOGINCOMMAND_H
