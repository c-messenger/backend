//
// Created by David on 2/15/2020.
//

#ifndef MESSENGER_SHOWONLINEUSERSCOMMAND_H
#define MESSENGER_SHOWONLINEUSERSCOMMAND_H

#include "../Command.h"
#include <string>
#include <iostream>
#include <map>

using namespace std;

class ShowOnlineUsersCommand : public Command {
private:

public:
    ShowOnlineUsersCommand(map<string, string> parameters);

    virtual void execute() noexcept override;
};


#endif //MESSENGER_SHOWONLINEUSERSCOMMAND_H
