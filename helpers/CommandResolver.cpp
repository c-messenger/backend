//
// Created by David on 2/11/2020.
//

#include "CommandResolver.h"
#include <iostream>
#include "commands/LoginCommand/LoginCommand.h"
#include "commands/InvalidCommand/InvalidCommand.h"
#include "commands/RegisterCommand/RegisterCommand.h"
#include "commands/ShowOnlineUsersCommand/ShowOnlineUsersCommand.h"
#include "commands/MessageCommand/MessageCommand.h"

using namespace std;

void CommandResolver::resolve(string fullCommand) {

    int state = 0;
    string word;
    string paramKey;
    bool bigStringStarted = false;

    string newCommand;
    for (int j = 0; j < fullCommand.length(); ++j) {
        if (fullCommand[j] != '\n' && fullCommand[j] != '\r')
            newCommand += fullCommand[j];
    }
    fullCommand = newCommand + ' ';

    for (int i = 0; i < fullCommand.length(); ++i) {

        if (fullCommand[i] == ' ' && !bigStringStarted) {
            if (state == 0) {
                command = word;
                state = 1;
            } else if (state == 1) {
                paramKey = word;
                state = 2;
            } else if (state == 2) {
                params.insert({paramKey, word});
                state = 1;
            }
            word = "";
        } else{
            if (state == 1) {
                if (word.length() == 0 && fullCommand[i] == '-') {

                } else {
                    if (fullCommand[i] == '"') {
                        bigStringStarted = !bigStringStarted;
                    }else{
                        word += fullCommand[i];
                    }
                }
            } else {
                if (fullCommand[i] == '"') {
                    bigStringStarted = !bigStringStarted;
                }else{
                    word += fullCommand[i];
                }

            }
        }
    }

}

Command *CommandResolver::getCommand() {
    if (command == "login") {
        return new LoginCommand(params);
    } else if (command == "register") {
        return new RegisterCommand(params);
    } else if (command == "online_users") {
        return new ShowOnlineUsersCommand(params);
    }  else if (command == "message") {
        return new MessageCommand(params);
    } else {
        return new InvalidCommand(params);
    }
}
