//
// Created by David on 2/14/2020.
//

#include "User.h"

int User::create(const map<string, boost::variant<string, int, float>> &data) {
    return insert(table, data);
}

bool User::update(int id, const map<string, boost::variant<string, int, float>> &data) {
    return Model::update(table, id, data);
}

sql::ResultSet *User::where(const map<string, boost::variant<string, int, float >> &data) {
    return Model::where(table, data);
}

