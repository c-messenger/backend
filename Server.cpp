//
// Created by David on 2/6/2020.
//

#include <iostream>
#include "Server.h"
#include "Session.h"
#include <boost/bind.hpp>

using namespace std;

Server::Server(boost::asio::io_service &io_service, short port) : io_service_(io_service),
                                                                  acceptor_(io_service, boost::asio::ip::tcp::endpoint(
                                                                          boost::asio::ip::tcp::v4(), port)) {
    startAccept();
}

void Server::startAccept() {
    auto *new_session = new Session(io_service_);

    new_session->setOnlineUsers(&onlineUsers);

    acceptor_.async_accept(new_session->socket(),
                           boost::bind(&Server::handleAccept, this, new_session,
                                       boost::asio::placeholders::error));
}

void Server::handleAccept(Session *new_session, const boost::system::error_code &error) {
    if (!error) {
        new_session->start();
    } else {
        cout<< "test"<<flush;

        delete new_session;
    }

    startAccept();
}
