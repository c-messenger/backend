//
// Created by David on 2/15/2020.
//

#include "MessageCommand.h"


MessageCommand::MessageCommand(map<string, string> parameters) : Command(parameters) {
    setParameter("to", &to, true);
    setParameter("message", &message, true);
}

void MessageCommand::execute() noexcept {
    auto it = session->pOnlineUsers->find(stoi(to));
    if (it != session->pOnlineUsers->end()) {
        sendMessage(it->second.second, it->second.first);
    } else {
        error = true;
        errorMessage = "User is not online";
    }
}

void MessageCommand::sendMessage(Session *messageSession, string name) {
    messageSession->sendMessage("m: New message From: " + session->userName + '\n' + message);
    result = "Message sent to: " + name;
}
