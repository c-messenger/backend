#include <boost/asio.hpp>

#include "Server.h"

using namespace std;

int main(int argc, char *argv[]) {
    boost::asio::io_service io_service;

    Server s(io_service, 8080);

    io_service.run();

    return 0;
}