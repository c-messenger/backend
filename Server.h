//
// Created by David on 2/6/2020.
//

#ifndef MESSENGER_SERVER_H
#define MESSENGER_SERVER_H

#include <boost/asio.hpp>
#include "Session.h"

class Server {
private:
    boost::asio::io_service &io_service_;
    boost::asio::ip::tcp::acceptor acceptor_;

    map<int, pair<string, Session*>> onlineUsers;
public:
    Server(boost::asio::io_service &io_service, short port);

private:
    void startAccept();

    void handleAccept(Session *new_session, const boost::system::error_code &error);

};

#endif //MESSENGER_SERVER_H
