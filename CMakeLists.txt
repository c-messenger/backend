cmake_minimum_required(VERSION 3.10)
project(messenger)

set(CMAKE_CXX_STANDARD 14)

add_definitions(-DBOOST_ERROR_CODE_HEADER_ONLY)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -pthread")

set(Boost_INCLUDE_DIR /home/davidnaj/boost_1_72_0)
set(Boost_LIBRARY_DIR /home/davidnaj/boost_1_72_0/stage/lib)

find_package(Boost COMPONENTS system filesystem REQUIRED)

include_directories(${Boost_INCLUDE_DIR})

add_executable(messenger main.cpp
        Server.cpp Server.h
        helpers/CommandResolver.cpp helpers/CommandResolver.h
        helpers/commands/Command.cpp helpers/commands/Command.h
        helpers/commands/LoginCommand/LoginCommand.cpp helpers/commands/LoginCommand/LoginCommand.h
        helpers/commands/InvalidCommand/InvalidCommand.cpp helpers/commands/InvalidCommand/InvalidCommand.h
        helpers/commands/RegisterCommand/RegisterCommand.cpp helpers/commands/RegisterCommand/RegisterCommand.h
        models/user/User.cpp models/user/User.h
        models/Model.cpp models/Model.h Session.cpp Session.h helpers/commands/ShowOnlineUsersCommand/ShowOnlineUsersCommand.cpp helpers/commands/ShowOnlineUsersCommand/ShowOnlineUsersCommand.h helpers/commands/MessageCommand/MessageCommand.cpp helpers/commands/MessageCommand/MessageCommand.h)
target_link_libraries(messenger mysqlcppconn)


