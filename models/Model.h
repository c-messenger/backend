//
// Created by David on 2/14/2020.
//

#ifndef MESSENGER_MODEL_H
#define MESSENGER_MODEL_H

#include <string>
#include <map>
#include <vector>
#include <boost/variant.hpp>
#include <cppconn/resultset.h>

using namespace std;

class Model {
private:

    string getKeysString(const map<string, boost::variant<string, int, float >>& = {});
    string getKeysString(vector<string>);

    string getValuesString(const map<string, boost::variant<string, int, float >>& = {});
    string getValuesString(vector<boost::variant<string, int, float >>);

    string getUpdateString(const map<string, boost::variant<string, int, float >>& = {});
    string getWhereString(const map<string, boost::variant<string, int, float >>& = {});
protected:
    int insert(string table, const map<string, boost::variant<string, int, float >>& = {});
    bool update(string table, int id, const map<string, boost::variant<string, int, float >>& = {});
    sql::ResultSet *where(string table, const map<string, boost::variant<string, int, float >>& = {});
public:
};


#endif //MESSENGER_MODEL_H
