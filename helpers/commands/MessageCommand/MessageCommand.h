//
// Created by David on 2/15/2020.
//

#ifndef MESSENGER_MESSAGECOMMAND_H
#define MESSENGER_MESSAGECOMMAND_H

#include <string>
#include "../Command.h"

using namespace std;

class MessageCommand : public Command {
private:
    string to;
    string message;
public:
    MessageCommand(map<string, string> parameters);

    virtual void execute() noexcept override;

    void sendMessage(Session* messageSession, string name);
};


#endif //MESSENGER_MESSAGECOMMAND_H
