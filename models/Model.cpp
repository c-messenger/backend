//
// Created by David on 2/14/2020.
//

#include "Model.h"
#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

#include <boost/variant.hpp>

int Model::insert(string table, const map<string, boost::variant<string, int, float>>& data) {
    sql::Driver *driver;
    sql::Connection *con = nullptr;
    sql::Statement *stmt = nullptr;
    sql::ResultSet *res = nullptr;

     try {

         driver = get_driver_instance();
         con = driver->connect("tcp://127.0.0.1:3306", "root", "root");
         con->setSchema("cpp_messenger");

         stmt = con->createStatement();

         string sql =
                 "INSERT INTO "+table+" "+getKeysString(data)+" VALUES "+getValuesString(data)+";";

         stmt->execute(sql);

         res = stmt->executeQuery("SELECT LAST_INSERT_ID() AS id;");
         if (res->next())
         {
             return stoi(res->getString("id"));
         }

         delete res;
         delete stmt;
         delete con;

         return 0;
     } catch (sql::SQLException &e) {
         cout << "# ERR: SQLException in " << __FILE__;
         cout << "(" << __FUNCTION__ << ") on line "
              << __LINE__ << endl;
         cout << "# ERR: " << e.what();
         cout << " (MySQL error code: " << e.getErrorCode();
         cout << ", SQLState: " << e.getSQLState() << " )" << endl;

         delete res;
         delete stmt;
         delete con;

         return 0;
     }
}


bool Model::update(string table, int id, const map<string, boost::variant<string, int, float >> &data) {

    sql::Connection *con = nullptr;
    sql::Statement *stmt = nullptr;

    try {
        sql::Driver *driver;

        driver = get_driver_instance();
        con = driver->connect("tcp://127.0.0.1:3306", "root", "root");
        con->setSchema("cpp_messenger");

        stmt = con->createStatement();

        string sql =
                "UPDATE "+table+" SET "+getUpdateString(data)+" WHERE id="+to_string(id)+";";

        stmt->execute(sql);

        delete stmt;
        delete con;

        return true;
    } catch (sql::SQLException &e) {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line "
             << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;

        delete stmt;
        delete con;

        return false;
    }
}


sql::ResultSet *Model::where(string table, const map<string, boost::variant<string, int, float >> &data) {
    sql::Connection *con;
    sql::Statement *stmt;
    sql::ResultSet *res = nullptr;

    try {
        sql::Driver *driver;

        driver = get_driver_instance();
        con = driver->connect("tcp://127.0.0.1:3306", "root", "root");
        con->setSchema("cpp_messenger");

        stmt = con->createStatement();

        string sql =
                "SELECT * FROM "+table+" WHERE "+getWhereString(data)+" ;";

        res = stmt->executeQuery(sql);

        delete stmt;
        delete con;

        return res;
    } catch (sql::SQLException &e) {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line "
             << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;


        delete stmt;
        delete con;
        delete res;

        return nullptr;
    }
}


string Model::getKeysString(const map<string, boost::variant<string, int, float >>& data) {
    vector<string> dataVector;
    for (auto elem : data)
        dataVector.push_back(elem.first);

    return getKeysString(dataVector);
}

string Model::getKeysString(vector<string> data) {
    string keys = "(";
    int i = 1;
    for (auto it = data.begin(); it != data.end(); it++) {
        keys += *it;
        if (i != data.size()) {
            keys += ", ";
        } else {
            keys += ")";
        }
        i++;
    }

    return keys;
}


string Model::getValuesString(const map<string, boost::variant<string, int, float >>& data) {
    vector<boost::variant<string, int, float >> dataVector;
    for (auto elem : data)
        dataVector.push_back(elem.second);

    return getValuesString(dataVector);
}


string Model::getValuesString(vector<boost::variant<string, int, float >> data) {
    string values = "(";
    int i = 1;
    for (auto it = data.begin(); it != data.end(); it++) {
        if (it->type() == typeid(std::string)) {
            values += '"';
            values += boost::get<std::string>(*it);
            values += '"';
        } else if (it->type() == typeid(int)) {
            values += std::to_string(boost::get<int>(*it));
        } else {
            values += boost::get<float>(*it);
        }
        if (i != data.size()) {
            values += ", ";
        } else {
            values += ")";
        }
        i++;
    }

    return values;
}

string Model::getUpdateString(const map<string, boost::variant<string, int, float >> &data) {
    string values;
    int i = 1;
    for (auto it = data.begin(); it != data.end(); it++) {
        values += it->first + "=";
        if (it->second.type() == typeid(std::string)) {
            values += '"';
            values += boost::get<std::string>(it->second);
            values += '"';
        } else if (it->second.type() == typeid(int)) {
            values += std::to_string(boost::get<int>(it->second));
        } else {
            values += boost::get<float>(it->second);
        }
        if (i != data.size()) {
            values += ", ";
        }
        i++;
    }

    return values;
}


string Model::getWhereString(const map<string, boost::variant<string, int, float >> &data) {
    string values;
    int i = 1;
    for (auto it = data.begin(); it != data.end(); it++) {
        values += it->first + "=";
        if (it->second.type() == typeid(std::string)) {
            values += '"';
            values += boost::get<std::string>(it->second);
            values += '"';
        } else if (it->second.type() == typeid(int)) {
            values += std::to_string(boost::get<int>(it->second));
        } else {
            values += boost::get<float>(it->second);
        }
        if (i != data.size()) {
            values += " AND ";
        }
        i++;
    }

    return values;
}
