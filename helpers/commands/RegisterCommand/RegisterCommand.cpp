
#include "RegisterCommand.h"
#include "mysql_connection.h"
#include "../../../models/user/User.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cstring>
#include <random>
#include <regex>

using namespace std;

RegisterCommand::RegisterCommand(map<string, string> parameters) : Command(parameters) {
    setParameter("name", &name, true);
    setParameter("email", &email, true);
    setParameter("password", &password, true);
}

void RegisterCommand::execute() noexcept {
    if (validateEmail()) {
        int userId = insertUser();
        if (userId != 0) {
            session->pOnlineUsers->insert({
                                                  userId,
                                                  {name, session}
                                          });
            session->userId = userId;
            session->userName = name;

            // send online users to all users
            for (auto it = session->pOnlineUsers->begin(); it != session->pOnlineUsers->end(); ++it) {
                if (it->first != userId) {

                    string onlineUsers;
                    for (auto it2 = session->pOnlineUsers->begin(); it2 != session->pOnlineUsers->end(); ++it2) {
                        if(it->first != it2->first)
                            onlineUsers += to_string(it2->first) + " => " + it2->second.first + '\n';
                    }

                    it->second.second->sendMessage("o: " + onlineUsers);
                }
            }

            result = "You are successfully registered";
        } else {
            error = true;
            errorMessage = "Something went wrong";
        }
    }

}


bool RegisterCommand::validateEmail() {
    User user;

    sql::ResultSet *res = user.where({
                                             {"email", email}
                                     });

    if (res->next()) {
        error = true;
        errorMessage = "Email has already been taken";
        delete res;
        return false;
    }

    const std::regex pattern
            ("^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$");
    bool isEmailValid = std::regex_match(email, pattern);

    if (!isEmailValid) {
        error = true;
        errorMessage = "Invalid email";
    }

    delete res;
    return isEmailValid;
}

int RegisterCommand::insertUser() {

    User user;

    return user.create({
                               {"name",     name},
                               {"email",    email},
                               {"password", password},
                       });
}

