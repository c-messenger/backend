//
// Created by David on 2/14/2020.
//

#ifndef MESSENGER_SESSION_H
#define MESSENGER_SESSION_H

#include <boost/asio.hpp>
#include <string>

using namespace std;

class Session {
private:
    boost::asio::ip::tcp::socket socket_;
    enum {
        max_length = 1024
    };
    char data_[max_length]{};

public:
    Session(boost::asio::io_service &io_service);

    boost::asio::ip::tcp::socket &socket();

    void start();

    void setOnlineUsers(map<int, pair<string, Session*>> *pOnUsers);

    map<int, pair<string, Session*>> *pOnlineUsers;

    int userId;
    string userName;

    void sendMessage(const string &messageText);

private:
    void readMessage();
    void readCallback(const boost::system::error_code &error, size_t bytes_transferred);

    void writeMessage(const string &messageText);
    void writeCallback(const boost::system::error_code &error);

    void removeData();
};


#endif //MESSENGER_SESSION_H
