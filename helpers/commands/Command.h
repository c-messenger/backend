//
// Created by David on 2/11/2020.
//

#ifndef MESSENGER_COMMAND_H
#define MESSENGER_COMMAND_H

#include <map>
#include <string>
#include "../../Session.h"

using namespace std;

class Command {
protected:
    string result;

    bool error = false;
    string errorMessage;

    map<string, string> params;

    Session *session;
public:
    Command(map<string, string> parameters);

    virtual void execute() = 0;

    void printParams();

    void setParameter(string key, string *parameter, bool required = false);

    string getResult();

    bool hasError();

    string getError();

    void setSession(Session *pSession);
};


#endif
